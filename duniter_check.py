#!/usr/bin/env python

import plyvel
import json

LEVEL_DB_PATH = "./leveldb"

# open copy of leveldb folder, copy made from a running Duniter server
db_identities = plyvel.DB(LEVEL_DB_PATH + '/level_iindex', )

# open copy of leveldb folder, copy made from a running Duniter server
db_certifications = plyvel.DB(LEVEL_DB_PATH + '/level_cindex', )

# open copy of leveldb folder, copy made from a running Duniter server
db_memberships = plyvel.DB(LEVEL_DB_PATH + '/level_mindex', )


def get_identity_from_db_entry(json_string: str) -> dict:
    """
    Get identity dict list from json string
    Parse list to get last updated status of identity

    :param json_string: Json entry
    :return:
    """
    identity_list = json.loads(json_string)  # type: list
    # update first identity properties except for None value
    return {key: value for list_item in identity_list for (key, value) in list_item.items() if value is not None or list_item == identity_list[0]}


def get_membership_from_db_entry(json_string: str) -> dict:
    """
    Get membership dict list from json string
    Parse list to get last updated status of membership

    :param json_string: Json entry
    :return:
    """
    membership_list = json.loads(json_string)  # type: list
    # update first membership properties except for None value
    return {key: value for list_item in membership_list for (key, value) in list_item.items() if value is not None or list_item == membership_list[0]}


def get_issuer_to_receiver_certification(issuer_pubkey: str, receiver_pubkey: str) -> dict:
    """
    Return updated certification dict of issuer to receiver

    :param issuer_pubkey: Issuer public key
    :param receiver_pubkey: Receiver public key
    :return:
    """
    # capture issuer certifications list
    issuer_certifications = json.loads(db_certifications.get(issuer_pubkey.encode()))
    # filter only certifications to receiver
    issuer_to_receiver_certifications = [issuer_certification for issuer_certification in issuer_certifications["issued"] if issuer_certification["receiver"] == receiver_pubkey]
    # update first certification properties except for None value
    return {key: value for issuer_certification in issuer_to_receiver_certifications for (key, value) in issuer_certification.items() if value is not None or issuer_certification == issuer_to_receiver_certifications[0]}


def check_membership():
    """
    Check membership integrity
    member = True and count(expired_on=0) > 4
    or
    member = False and count(expired_on=0) < 5

    :return:
    """
    print("check membership...")

    for key, json_string in db_identities:
        receiver_identity = get_identity_from_db_entry(json_string)
        receiver_membership = get_membership_from_db_entry(db_memberships.get(key))

        # if receiver_identity["uid"] == "Laween":
        #     # print(receiver_membership)
        #     # print(receiver_identity)

        receiver_certifications = json.loads(db_certifications.get(key))

        count_valid = 0
        issuers_uid = []
        for issuer_key in receiver_certifications["received"]:
            issuer_identity = get_identity_from_db_entry(db_identities.get(issuer_key.encode()))
            issuer_certification = get_issuer_to_receiver_certification(issuer_key, receiver_identity["pub"])
            # print(issuer_certification)
            if len(issuer_certification) > 0 and issuer_certification["expired_on"] == 0 and issuer_identity["uid"] not in issuers_uid:
                # print(issuer_identity["uid"])
                issuers_uid.append(issuer_identity["uid"])
                count_valid += 1

        if receiver_identity["member"] is True and count_valid < 5:
            print(receiver_identity["uid"], receiver_identity["member"], receiver_identity["pub"])
            print(f"only {count_valid} valid certifications but identity is member !")
            break

        if receiver_identity["member"] is True and (receiver_membership["expired_on"] != 0 and receiver_membership["expired_on"] is not None):
            print(receiver_identity["uid"], receiver_identity["member"], receiver_identity["pub"])
            print(f"membership expired but identity is member !")
            print(receiver_membership)
            break

        if receiver_identity["member"] is True and receiver_membership["revoked_on"] is not None:
            print(receiver_identity["uid"], receiver_identity["member"], receiver_identity["pub"])
            print(f"membership revoked but identity is member !")
            break

        if receiver_identity["member"] is False and (receiver_membership["expired_on"] is None or receiver_membership["expired_on"] == 0) and count_valid > 4 and receiver_membership["revoked_on"] is None:
            print(receiver_identity["uid"], receiver_identity["member"], receiver_identity["pub"])
            print(f"membership non expired but identity is not member !")
            print(receiver_membership)
            print(issuers_uid)
            break


if __name__ == '__main__':

    check_membership()
