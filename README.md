# duniter_check

A python script to check data integrity in Duniter V1 LevelDB.

Requires Python 3.10+.

Code licenced under GPL v3.

## Dependencies

* plyvel package

## Installation

Clone the git repository

    git clone https://git.duniter.org/tools/duniter_check

### Install dependencies

If your are using a virtual env : 

pip install -r requirements.txt

Otherwise, use `--user` option : 

pip install --user -r requirements.txt

### Copy levelDB folder in the script folder

    rsync -r duniter_server_hostname:/path/to/folder/levelDB .

## Usage

    ./duniter_check.py
